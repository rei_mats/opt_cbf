#ifndef FACT_HPP
#define FACT_HPP

namespace fact {
  int Factorial(int n);
  int DoubleFactorial(int n);
}

#endif
